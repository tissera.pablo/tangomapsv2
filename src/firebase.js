import { initializeApp } from 'firebase'

const app = initializeApp({
  apiKey: 'AIzaSyBlwRM-6BVdjt7VdrZUlhxXTS5r6L-zadw',
  authDomain: 'tangomaps-144f4.firebaseapp.com',
  databaseURL: 'https://tangomaps-144f4.firebaseio.com',
  projectId: 'tangomaps-144f4',
  storageBucket: 'tangomaps-144f4.appspot.com',
  messagingSenderId: '705256876667'
})

export const db = app.database();
export const tangoSitiosRef = db.ref('TangoSitios');
export const tangoEventosRef = db.ref('TangoEventos');
