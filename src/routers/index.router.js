import Vue from 'vue'
import VueRouter from 'vue-router'
import LandingPage from '../components/LandingPage.vue'
import AboutPage from '../components/AboutPage.vue'
import ContactPage from '../components/ContactPage.vue'
import GuidePage from '../components/GuidePage.vue'
import VolunteerPage from '../components/VolunteerPage.vue'
import FAQPage from '../components/FAQPage.vue'
const Profile = () => import('@/components/User/Profile')
const Signup = () => import('@/components/User/Signup')
const Signin = () => import('@/components/User/Signin')
import AuthGuard from './auth-guard'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  base: __dirname,  
  routes: [
    {
      path: '/',
      name: 'Home',
      component: LandingPage
    },
    {
      path: '/about',
      name: 'About',
      component: AboutPage
    },
    {
      path: '/contact',
      name: 'Contact',
      component: ContactPage
    },
    {
      path: '/guide',
      name: 'Guide',
      component: GuidePage
    },
    {
      path: '/volunteer',
      name: 'Volunteer',
      component: VolunteerPage
    },
    {
      path: '/faq',
      name: 'FAQ',
      component: FAQPage
    },
    {
      path: '/profile',
      name: 'Profile',
      component: Profile,
      beforeEnter: AuthGuard
    },
    {
      path: '/signup',
      name: 'Signup',
      component: Signup
    },
    {
      path: '/signin',
      name: 'SignIn',
      component: Signin
    },
    // otherwise redirect to home
    { 
      path: '*',
      redirect: '/' 
    }
  ]
})

export default router;