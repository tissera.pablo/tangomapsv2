import backendApiServices from "./backendApi.service";


const mapsManagerService = {}

mapsManagerService.getAll = function() {
  return backendApiServices.get('/map/getAll')
}

export default mapsManagerService