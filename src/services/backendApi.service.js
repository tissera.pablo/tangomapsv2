import trae from 'trae'
import configService from './config'

const backendApiServices = trae.create({
  baseUrl: configService.baseApiUrl
})

export default backendApiServices