import Vue from 'vue'
import App from './App.vue'
import * as VueGoogleMaps from 'vue2-google-maps';
import GmapCluster from 'vue2-google-maps/dist/components/cluster'
import VueFire from 'vuefire'
import './firebase'
import firebase from 'firebase/app'
import 'firebase/firestore'
import router from './routers/index.router'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faTwitter, faFacebook, faLinkedin, faInstagram, faGoogle} from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon, FontAwesomeLayers, FontAwesomeLayersText } from '@fortawesome/vue-fontawesome'
import { store } from './store'
import UUID from 'vue-uuid';

window._ = require('lodash');

library.add(
  faTwitter,
  faFacebook,
  faLinkedin,
  faInstagram,
  faGoogle
  )

Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.component('font-awesome-layers', FontAwesomeLayers)
Vue.component('font-awesome-layers-text', FontAwesomeLayersText)

Vue.config.productionTip = false

Vue.use(VueFire)
Vue.use(UUID);
Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyDJ105_lFA2XZQ9QcVALMl1814I8lmHicI",
    libraries: "places" // necessary for places input
  }
});

Vue.component('gmap-cluster', GmapCluster);

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router,
  store,
  created () {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.$store.dispatch('autoSignIn', user)
      }
    })
  }
}).$mount('#app')
