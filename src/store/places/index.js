export default {
  state: {
    places: []
  },
  mutations: {
    setPlaces (state, payload) {
      state.places = payload
    }
  },
  actions: {    
    loadPlaces ({commit}, payload) {
      commit('setPlaces', {
        payload
      })
    }
  },
  getters: {
    places (state) {
      return state.places
    }
  }
}
